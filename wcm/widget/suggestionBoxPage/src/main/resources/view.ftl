<div id="MyWidget_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide" data-params="MyWidget.instance()">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- If you delete this meta tag, Half Life 3 will never be released. -->
		<meta name="viewport" content="width=device-width" />

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Detalhes da sugestão</title>
			
		<link rel="stylesheet" type="text/css" href="stylesheets/email.css" />

		<style>
			* { 
			margin:0;
			padding:0;
		}
		* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

		img { 
			max-width: 100%; 
		}
		.collapse {
			margin:0;
			padding:0;
		}
		body {
			-webkit-font-smoothing:antialiased; 
			-webkit-text-size-adjust:none; 
			width: 100%!important; 
			height: 100%;
		}
		
		.maxWidth {
			max-width: 1024px !important;
		}

		/* ------------------------------------- 
				ELEMENTS 
		------------------------------------- */
		a { color: #2BA6CB;}

		.btn {
			text-decoration:none;
			color: #FFF;
			background-color: #666;
			padding:10px 16px;
			font-weight:bold;
			margin-right:10px;
			text-align:center;
			cursor:pointer;
			display: inline-block;
		}

		p.callout {
			padding:15px;
			background-color:#ECF8FF;
			margin-bottom: 15px;
		}
		.callout a {
			font-weight:bold;
			color: #2BA6CB;
		}

		table.social {
		/* 	padding:15px; */
			background-color: #ebebeb;
			
		}
		.social .soc-btn {
			padding: 3px 7px;
			font-size:12px;
			margin-bottom:10px;
			text-decoration:none;
			color: #FFF;font-weight:bold;
			display:block;
			text-align:center;
		}
		a.fb { background-color: #3B5998!important; }
		a.tw { background-color: #1daced!important; }
		a.gp { background-color: #DB4A39!important; }
		a.ms { background-color: #000!important; }

		.sidebar .soc-btn { 
			display:block;
			width:100%;
		}

		/* ------------------------------------- 
				HEADER 
		------------------------------------- */
		table.head-wrap { width: 100%;}

		.header.container table td.logo { padding: 15px; }
		.header.container table td.label { padding: 15px; padding-left:0px;}

		/* ------------------------------------- 
				BODY 
		------------------------------------- */
		table.body-wrap {
		 width: 100%; 
		 border-left 2px solid #f3f3f3;
		  border-right 2px solid #f3f3f3; 
		  border-bottom 2px solid #f3f3f3;
		}

		/* ------------------------------------- 
				FOOTER 
		------------------------------------- */
		table.footer-wrap { width: 100%;	clear:both!important;
		}
		.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
		.footer-wrap .container td.content p {
			font-size:10px;
			font-weight: bold;
			
		}

		/* ------------------------------------- 
				TYPOGRAPHY 
		------------------------------------- */
		h1,h2,h3,h4,h5,h6 {
		font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
		}
		h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

		h1 { font-weight:200; font-size: 44px;}
		h2 { font-weight:200; font-size: 37px;}
		h3 { font-weight:500; font-size: 27px;}
		h4 { font-weight:500; font-size: 23px;}
		h5 { font-weight:900; font-size: 17px;}
		h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

		.collapse { margin:0!important;}

		p, ul { 
			margin-bottom: 10px; 
			font-weight: normal; 
			font-size:14px; 
			line-height:1.6;
		}
		p.lead { font-size:17px; }
		p.last { margin-bottom:0px;}

		ul li {
			margin-left:5px;
			list-style-position: inside;
		}

		/* ------------------------------------- 
				SIDEBAR 
		------------------------------------- */
		ul.sidebar {
			background:#ebebeb;
			display:block;
			list-style-type: none;
		}
		ul.sidebar li { display: block; margin:0;}
		ul.sidebar li a {
			text-decoration:none;
			color: #666;
			padding:10px 16px;
		/* 	font-weight:bold; */
			margin-right:10px;
		/* 	text-align:center; */
			cursor:pointer;
			border-bottom: 1px solid #777777;
			border-top: 1px solid #FFFFFF;
			display:block;
			margin:0;
		}
		ul.sidebar li a.last { border-bottom-width:0px;}
		ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}

		/* --------------------------------------------------- 
				RESPONSIVENESS
				Nuke it from orbit. It's the only way to be sure. 
		------------------------------------------------------ */

		/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
		.container {
			display:block!important;
			max-width:1024px!important;
			margin:0 auto!important; /* makes it centered */
			clear:both!important;
		}

		/* This should also be a block element, so that it will fill 100% of the .container */
		.content {
			padding:15px;
			max-width:1024px;
			margin:0 auto;
			display:block; 
		}

		/* Let's make sure tables in the content area are 100% wide */
		.content table { width: 100%; }


		/* Odds and ends */
		.column {
			width: 300px;
			float:left;
		}
		.column tr td { padding: 15px; }
		.column-wrap { 
			padding:0!important; 
			margin:0 auto; 
			max-width:600px!important;
		}
		.column table { width:100%;}
		.social .column {
			width: 280px;
			min-width: 279px;
			float:left;
		}

		/* Be sure to place a .clear element after each set of columns, just to be safe */
		.clear { display: block; clear: both; }

		/* ------------------------------------------- 
				PHONE
				For clients that support media queries.
				Nothing fancy. 
		-------------------------------------------- */
		@media only screen and (max-width: 600px) {
			
			a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

			div[class="column"] { width: auto!important; float:none!important;}
			
			table.social div[class="column"] {
				width:auto!important;
			}

		}

		.bold{
			font-weight:bold;
		}


		table.dados {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
		}

		table.dados td, th {
			border: 1px solid #ffffff;
			text-align: left;
			padding: 8px;
		}

		table.dados tr.even {
			background-color: #000000;
		}
		.th-img{
			text-align:center;
		}

		.content h6, .content h5{
			display:inline-block;
			margin-right: 20px;
		}
		
		th{
			color: white !important;
			width: 20%;
			height: 50px;
			font-size: 12px;
		}
		
		</style>
	</head>
	<body bgcolor="#FFFFFF">
		<div class="maxWidth" align="center" style="margin:0px auto;">
			<!-- HEADER -->
			<table class="head-wrap" bgcolor="#ffffff">
				<tr>
					<td></td>
					<td class="header " style="margin-right: 200px!important" >				
						<div class="content">
							<table bgcolor="#ffffff">
								<tr>
									<td style="overflow-wrap: break-word;padding:20px 20px 0px 10px;" align="left">
									  <table width="100%" cellpadding="0" cellspacing="0" border="0">
										 <tr>
											<td style="padding-right: 0px; padding-left: 0px;" align="left">
											   <!-- INÍCIO DA LOGO - CABEÇALHO -------------------------------------------------------------------------------------------------->
											   <img align="left" border="0" src="http://fluig.konicaminolta.com.br/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD02MzI3OSZ2ZXI9MTAwMCZmaWxlPWtvbmljYV9ob3Jpem9udGFsLnBuZyZjcmM9NDM0MTY5NjEwJnNpemU9MC4wMTczNjMmdUlkPTMxOCZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9ZmFsc2U=.png" alt="Image" title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;width: 100%;max-width: 270px;" width="270">
											   <!-- FIM DA LOGO - CABEÇALHO ----------------------------------------------------------------------------------------------------->
											</td>
										 </tr>
									  </table>
								   </td>
									<td style="overflow-wrap: break-word;padding:31px 20px 0px;" align="left">
									  <!-- INÍCIO DA DATA DE ENVIO - CABEÇALHO ------------------------------------------------------------------------------------------------->
									  <div style="color: #000; line-height: 150%; text-align: right; font-family: inherit; margin-top:10px;">
										 <p style="font-size: 14px; line-height: 150%;">
											 <span style="font-family: Lato, sans-serif; font-size: 14px; line-height: 21px; color: #7d7d7d;">
												<span id='dataEnvio' style="line-height: 21px; font-size: 14px;"></span>
											 </span>
										 </p>
									  </div>
									  <!-- FIM DA DATA DE ENVIO - CABEÇALHO ---------------------------------------------------------------------------------------------------->
								   </td>						
								</tr>
							</table>
							<div style="text-align: center; margin-top: 10px; margin-bottom: 15px;">
								<div style="border-top-width: 1px; border-top-style: solid; border-top-color: #CCC; display: inline-block; width: 100%; line-height: 1px; font-size: 1px; height: 1px;">&#160;</div>
							</div>
						</div>	
					</td>
					<td></td>
				</tr>
			</table>
			<!-- /HEADER -->

			<!-- BODY -->
			<table class="body-wrap">
				<tr>
					<td></td>
					<td class="container" bgcolor="#FFFFFF">
						<!-- CONTENT -->
						<div class="content">
							<div style="color: #303030; line-height: 120%; text-align: left; font-family: inherit; margin-bottom:20px;">
								<p style="font-size: 13px; line-height: 120%; text-align: center;">
									<!-- INÍCIO DA MENSAGEM 1 - CORPO DO E-MAIL --------------------------------------------------------------------------------->
									<span style="font-size: 20px; line-height: 24px;">
										DETALHES DA SUGESTÃO
									</span>
									<!-- FIM DA MENSAGEM 1 - CORPO DO E-MAIL ------------------------------------------------------------------------------------>
								</p>
							</div>
							<div id='tbodyContent'>
								<table class='dados'>
									<thead>
										<tr class='even'>
											<th> CÓDIGO</th>
											<th> DATA</th>
											<th> NOME</th>
											<th> DESCRIÇÃO</th>
											<th> CONTRIBUIÇÃO P/ EMPRESA</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							
							<div style="text-align: left;">
								<div style="border-top-width: 1px; border-top-style: dotted; border-top-color: #CCC; display: inline-block; width: 100%; line-height: 1px; font-size: 1px; height: 1px;">&#160;</div>	
							</div>
							<br>
							<div class="email-col-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
							  <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
								 <tbody>
									<tr>
									   <td style="overflow-wrap: break-word;padding:20px 20px 35px;" align="left">
										  <div style="color: #303030; line-height: 170%; text-align: center; font-family: inherit;">
											 <p style="font-size: 12px; line-height: 170%;">O seu sigilo foi respeitado. A identificação é opcional!</p>
										  </div>
									   </td>
									</tr>
								 </tbody>
							  </table>
							  <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
								 <tbody>
									<tr>
									   <td style="overflow-wrap: break-word;padding:5px;" align="left">
										  <div style="color: #7d7d7d; line-height: 140%; text-align: center; font-family: inherit;">
											 <p style="font-size: 14px; line-height: 140%;"><span style="font-size: 12px; line-height: 16.8px;"><a title="Fluig - Konica Minolta" href="http://fluig.konicaminolta.com.br" target="_blank" rel="noopener"><span style="text-decoration: underline; line-height: 16.8px; font-size: 12px;">Fluig</span> </a>|&nbsp;<a title="Site - Konica Minolta" href="http://www.konicaminolta.com.br" target="_blank" rel="noopener"><span style="text-decoration: underline; line-height: 16.8px; font-size: 12px;">Sobre n&oacute;s</span></a></span></p>
											 <p style="font-size: 14px; line-height: 140%;">&nbsp;</p>
											 <p style="font-size: 14px; line-height: 140%;"><span style="font-size: 12px; line-height: 16.8px;">Copyright &copy; 2018 Konica Minolta. Todos os direitos reservados.&nbsp;</span></p>
										  </div>
									   </td>
									</tr>
								 </tbody>
							  </table>
						   </div>
						</div>
						<!-- /CONTENT -->						
					</td>
					<td></td>
				</tr>
			</table>
		</div>
		<!-- /BODY -->
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				getDataAtual();
				
				var codSuggestion = capturaParametro()["id"];
				
				if(codSuggestion){
					var objJSON = getDataSuggestion(codSuggestion);
					createHtmlTable(objJSON);
				}else{
					alert("Não foi possível identificar o código da sugestão!");
				}
								
				/* FUNÇÕES */
				// Monta tabela html
				function createHtmlTable(objJSON){
					var htmlContent = "<tr>" +
							    "<td style='overflow-wrap: break-word;padding:15px 20px; width:20%;' align='left'>" +
									"<div class='email-col-25 outlook-group-fix' style='color: #303030; line-height: 120%; text-align: left; font-family: inherit;'>" +
										"<p style='font-size: 12px; line-height: 120%;'>" + objJSON.id + "</p>" +
									"</div>" +
								"</td>" +
    							"<td style='overflow-wrap: break-word;padding:15px 20px; width:20%;' align='left'>" +
    								"<div class='email-col-25 outlook-group-fix' style='color: #303030; line-height: 120%; text-align: left; font-family: inherit;'>" +
    									"<p style='font-size: 12px; line-height: 120%;'>" + objJSON.dateSendSuggestion + "</p>" +
    								"</div>" +
    							"</td>" +
    							"<td style='overflow-wrap: break-word;padding:15px 20px; width:20%;' align='left'>" +
    								"<div class='email-col-25 outlook-group-fix' style='color: #303030; line-height: 120%; text-align: left; font-family: inherit;'>" +
    									"<p style='font-size: 12px; line-height: 120%;'>" + objJSON.name + "</p>" +
    								"</div>" +
    							"</td>" +
    							"<td style='overflow-wrap: break-word;padding:15px 20px; width:20%;' align='left'>" +
    								"<div class='email-col-25 outlook-group-fix' style='color: #303030; line-height: 120%; text-align: center; font-family: inherit;'>" +
    									"<p style='font-size: 12px; line-height: 120%;'>" + objJSON.description + "</p>" +
    								"</div>" +
    							"</td>" +
    							"<td style='overflow-wrap: break-word;padding:15px 20px; width:20%;' align='left'>" +
    								"<div class='email-col-25 outlook-group-fix' style='color: #303030; line-height: 120%; text-align: center; font-family: inherit;'>" +
    									"<p style='font-size: 12px; line-height: 120%;'>" + objJSON.whySuggestion + "</p>" +
    								"</div>" +
    							"</td>" +
    						"</tr>";
					
					$('#tbodyContent table tbody').html(htmlContent);
				}
				
				// Busca as informações da sugestão
				function getDataSuggestion(codSuggestion){
				
					// Envia a sugestão para o serviço que salva no banco de dados
					paramsSendBase =  {
						"id" : codSuggestion
					};
					console.log(paramsSendBase);
					var retorno = Ajax("http://fluigh.konicaminolta.com.br:8081/suggestionBox/find", JSON.stringify(paramsSendBase), 'post');
					return retorno;
				}
				
				// Captura os parâmetros enviados via GET pela URL
				function capturaParametro(){
					var vars = [], hash;
					var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
					
					for(var i = 0; i < hashes.length; i++){
						hash = hashes[i].split('=');
						vars.push(hash[0]);
						vars[hash[0]] = hash[1];
					}
					
					return vars;
				}
				
				// Gera a data de acesso á página
				function getDataAtual(){
					//Gera data Atual
					var today = new Date();    
					var year = today.getFullYear();     
					var month = today.getMonth() + 1 < 10 ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1);    
					var day = today.getDate() < 10 ? '0' + today.getDate() : today.getDate();
					var dtAtual = day + '/' + month  + '/' + year;
					$('#dataEnvio').html(dtAtual);
				}
				
				// Realiza requisições ajax
				function Ajax(url, params, requestType){
					var retorno = null;
					$.ajax({
						type: requestType,
						url:  url,
						data: params,
						contentType:"application/json; charset=uft-8",
						dataType:"json",
						async:false,
						cache: false,
						success: function(data,textStatus,jqXHR){
							retorno = data;
							console.log(retorno);
						},
						error: function (jqXHR,textStatus,errorThrown){
							console.log(jqXHR);
							console.log(textStatus);
							console.log(errorThrown);
						}
					});
					
					return retorno;
				}
			});
		</script>
	</body>
</html>
</div>

