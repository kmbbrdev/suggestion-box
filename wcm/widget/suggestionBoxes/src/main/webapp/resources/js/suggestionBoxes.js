var dataAtual;
var dataInvertida;
var nameSugg;
var dateSugge;
var suggestion;
var whySuggestion;

var suggestionBoxes = SuperWidget.extend({
    //variáveis da widget
    variavelNumerica: null,
    variavelCaracter: null,
    currentUser		: null,
    emailUser		: null,
    urlServer		: null,

    //método iniciado quando a widget é carregada
    init: function() {
    	// Recuperando o usuário logado
    	this.currentUser = WCMAPI.userCode;
    	
    	// Busca o e-mail do usuário logado
		this.emailUser = this.getEmailUser(this.currentUser);
		
		// Busca a URL do servidor
		this.urlServer = window.location.origin;
		
    },
  
    //BIND de eventos
    bindings: {
        local: {
            'abrirModal': ['click_abrirModal'],
        },
        global: {
        	'sendData' : ['click_sendData'],
        }
    },
 
    executeAction: function(htmlElement, event) {
    },
    
    getEmailUser: function(userId){
    	var emailUser = '';
    	
    	var constraint = DatasetFactory.createConstraint("colleaguePK.colleagueId", userId, userId, ConstraintType.MUST);
    	var dsUserEmail = DatasetFactory.getDataset("colleague", new Array('mail'), new Array(constraint), null);
    	
    	if (dsUserEmail.values.length > 0) {
    		emailUser = dsUserEmail.values[0].mail;
    	}
    	
    	return emailUser;
    },
    
    abrirModal : function (){
    	var dataAtual = new Date();
    	var day = dataAtual.getDate() < 10 ? '0' + dataAtual.getDate() : dataAtual.getDate();
		var month = dataAtual.getMonth() < 10 ? '0' + (dataAtual.getMonth() + 1) : (dataAtual.getMonth() + 1);    	
    	var year = dataAtual.getFullYear();
    	
    	dataAtual = day + '/' + month  + '/' + year;
    	dataInvertida = year + '-' + month + '-' + day;	
    		
		//criando formulário de sugestão
		htmlSuggestion = '<div class="form-group">'
        htmlSuggestion += '<label for="nomeSuggestion">Nome (Opcional):</label>'
        htmlSuggestion += '<input type="text" class="form-control i" id="nomeSuggestion" placeholder="Insira o seu nome (Opcional)">'
        htmlSuggestion += '</div>'
        htmlSuggestion += '<div class="form-group">'
        htmlSuggestion += '<label for="dataSuggestion">Data:</label>'
        htmlSuggestion += '<input type="text" class="form-control" name="dataSuggestion" id="dataSuggestion" readonly>'
        htmlSuggestion += '</div>'
        htmlSuggestion += '<div class="form-group">'
        htmlSuggestion += '<label for="suggestion">Descreva abaixo a sua sugestão</label>'
        htmlSuggestion += '<textarea rows="5" cols="15" class="form-control i" id="suggestion" onkeyup="mostrarResultado(this.value,1000,\'spcontando19\');contarCaracteres(this,this.value,1000,\'sprestante19\')"></textarea>'
        htmlSuggestion += '	<span id="spcontando19" style="font-family:Georgia;">Ainda não temos nada digitado..</span><br /> <span id="sprestante19" style="font-family:Georgia;"></span>'		
        htmlSuggestion += '</div>'
        htmlSuggestion += '<div class="form-group">'	
        htmlSuggestion += '<label for="exampleInputFile">Como a sua idéia pode contribuir para a empresa?(Opcional)</label>'
        htmlSuggestion += '<textarea rows="3" cols="15" class="form-control i" id="whySuggestion" onkeyup="mostrarResultado(this.value,1000,\'spcontando18\');contarCaracteres(this,this.value,1000,\'sprestante18\')"></textarea>'  
        htmlSuggestion += '	<span id="spcontando18" style="font-family:Georgia;">Ainda não temos nada digitado..</span><br /> <span id="sprestante18" style="font-family:Georgia;"></span>'	
        htmlSuggestion += '</div>'
        	
	    var myModal = FLUIGC.modal({
	        title: 'Caixa de Sugestão',
	        content: htmlSuggestion,
	        id: 'fluig-modal',
	        actions: [{
	            'label': 'Enviar',
	            'bind': 'data-sendData',
	        },{
	            'label': 'Close',
	            'autoClose': true
	        }]
	    }, function(err, data) {
	        if(err) {
	            // do error handling
	        } else {
	            // do something with data
	        }
	    });
		
		$('#dataSuggestion').val(dataAtual);
    },
    
	getRoles: function (){
	     var retorno = Ajax("/portal/api/rest/wcm/service/role/findRoleUsers", "space=&roleCode=suggbox", 'get');
	     return retorno;
	},	
	    
    sendEmail : function(){
    	var receiver = "";
    	/*var users = this.getRoles();
    	console.log(users);
    	if(users.content.length > 0){
    		for(x = 0; x < users.content.length; x++){  		
    			receiver += users.content[x].email +";";	
    		};    		
    	};*/
    	
    	receiver = "bbrsm-suggestion@gcp.konicaminolta.com;";
    	
    	nameSugge = $("#nomeSuggestion").val();
    	
    	if(nameSugge ==""){
    		nameSugge = "Not Informad"
    	}else{
    		nameSugge = $("#nomeSuggestion").val();
    	};
    	
    	dateSugge = $("#dataSuggestion").val();    	
    	suggestion = $("#suggestion").val();
    	whySuggestion = $("#whySuggestion").val();
    	
    	if(whySuggestion ==""){
    		whySuggestion = "Not Informed"
    	}else{
    		whySuggestion = $("#whySuggestion").val();
    	};
    	
    	// Envia um e-mail para o grupo que irá avaliar as sugestões
    	var paramsTemplate = {
			"PARAM_nameSugg": nameSugge,//pega o nome do option
			"PARAM_dateSugge" : dateSugge,//pega o nome do solicitante
			"PARAM_Suggestion" : suggestion,
			"PARAM_WhySuggestion" : whySuggestion,
		};
    	
    	var params =  {
	 		"to" : "" + receiver, //emails of recipients separated by ";"
	 		"from" : "fluigdev@konicaminolta.com.br",// sender
	 		"subject" : "Suggestion Box", //subject
	 		"templateId" : "Suggestion_Box", //Email template Id previously registered
	 		"dialectId"  : "pt_BR", //Email dialect , if not informed receives pt_BR , email dialect ("pt_BR", "en_US", "es")
	 		"param" : paramsTemplate //Map with variables to be replaced in the template
    	};

    	Ajax("/api/public/alert/customEmailSender", JSON.stringify(params), 'post');
    	
    	// Envia a sugestão para o serviço que salva no banco de dados
    	paramsSendBase =  {
			"id" : "0",
			"name" : nameSugge,
			"description": suggestion,
			"whySuggestion" : whySuggestion,
			"dateSendSuggestion" :dataInvertida
    	};
    	
    	//var retorno = Ajax("http://fluig.konicaminolta.com.br/suggestionBox/send", JSON.stringify(paramsSendBase), 'post');
    	var retorno = Ajax("/suggestionBox/send", JSON.stringify(paramsSendBase), 'post');
    	
    	// Envia e-mail de feedback para o usuário que está enviando a sugestão
    	// "http://fluigh.konicaminolta.com.br:8081/portal/p/1/suggestionbox?id="
    	var paramsTemplateFeedback = {
			"DATA_ENVIO" : $('#dataSuggestion').val(),
			"URL_FEEDBACK" : this.urlServer + "/portal/p/1/suggestionbox?id=" + retorno.id
    	};
    	
    	var paramsFeedback = {
			"to" : "" + this.emailUser, //emails of recipients separated by ";"
	 		"from" : "fluigdev@konicaminolta.com.br",// sender
	 		"subject" : "Suggestion Box Feedback", //subject
	 		"templateId" : "Suggestion_Box_Feedback", //Email template Id previously registered
	 		"dialectId"  : "pt_BR", //Email dialect , if not informed receives pt_BR , email dialect ("pt_BR", "en_US", "es")
	 		"param" : paramsTemplateFeedback //Map with variables to be replaced in the template
    	};
    	
    	Ajax("/api/public/alert/customEmailSender", JSON.stringify(paramsFeedback), 'post');
    },
    
    sendData : function(){
    	
    	if($("#suggestion").val() == null || $("#suggestion").val() == ""){
		    FLUIGC.toast({
		        title: 'Estamos quase lá.',
		        message: 'É necesário informar a sua sugestão.',
		        type: 'danger'
		    });
		    
    	}else{
    		
    		this.sendEmail();
    		
		    FLUIGC.toast({
		        title: 'Tudo certo!',
		        message: 'Sua Sugestão Foi Enviada Com Sucesso',
		        type: 'success'
		    });
		    
    		$(".i").val("");
    	}
    },
});

function Ajax(url, params, requestType){
	var retorno = null;
	$.ajax({
		type: requestType,
		url:  url,
		data: params,
		contentType:"application/json; charset=uft-8",
		dataType:"json",
		async:false,
		cache: false,
		success: function(data,textStatus,jqXHR){
			retorno = data;
			console.log(retorno);
		},
		error: function (jqXHR,textStatus,errorThrown){
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
	
	return retorno;
}

function mostrarResultado(box,num_max,campospan){
	var contagem_carac = box.length;
	if (contagem_carac != 0){
		document.getElementById(campospan).innerHTML = contagem_carac + " caracteres digitados";
		if (contagem_carac == 1){
			document.getElementById(campospan).innerHTML = contagem_carac + " caracter digitado";
		}
		if (contagem_carac >= num_max){
			document.getElementById(campospan).innerHTML = "Limite de caracteres excedido!";
		}
	}else{
		document.getElementById(campospan).innerHTML = "Ainda não temos nada digitado..";
	}
}

function contarCaracteres(e,box,valor,campospan){
	var conta = valor - box.length;
	document.getElementById(campospan).innerHTML = "Você ainda pode digitar " + conta + " caracteres";
	if(box.length >= valor){
		document.getElementById(campospan).innerHTML = "Opss.. você não pode mais digitar..";
		e.value = e.value.substr(0,valor);
	}	
}
